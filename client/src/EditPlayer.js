import React, { Component } from 'react';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const EDIT_PLAYER = gql`
  mutation EditPlayer($name: String!, $position: String!) {
    editPlayer(name: $name, position: $position) {
      id
    }
  }
`;

class EditPlayer extends Component {
  someFunction(data) {
    //decide what to do with data here
  }

  render() {
    let input;

    return (
      <Mutation mutation={EDIT_PLAYER}>
        {(editPlayer, { data }) => (
          <div>
            <h2><span onClick={this.props.goBack} style={{cursor: "pointer"}}>&larr;</span> {this.props.player.name}</h2>
            <form onSubmit={e => {
              e.preventDefault();
              editPlayer({ variables: { name: this.props.player.name, position: input[input.selectedIndex].value }});
              this.props.reset();
            }}>
              <select ref={node => {
                input = node;
              }}>
                <option value="">Pick a position</option>
                <option value="C">Center</option>
                <option value="L">Left Wing</option>
                <option value="R">Right Wing</option>
                <option value="D">Defense</option> 
                <option value="G">Goalie</option>
              </select>
                <br />
              <button type="submit">Edit Player</button>
            </form>
          </div>
        )}
      </Mutation>
    );
  }
}

export default EditPlayer;
