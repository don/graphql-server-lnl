import React, { Component } from 'react';
import { ApolloProvider } from "react-apollo";
import Teams from "./Teams";
import Players from "./Players";
import EditPlayer from "./EditPlayer";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player: {},
      showPlayer: false,
      city: '' 
    };
  }

  changeCity = (city) => {
    this.setState({city}); 
  }

  changePlayer = (player) => {
    this.setState({player, lastCity: this.state.city, city: '', showPlayer: true});
  }

  resetPlayerView = () => {
    alert("Updated player's position");
    this.setState({player: {}, showPlayer: false});
  }

  goHome = () => {
    this.setState({player: {}, showPlayer: false, city: ''});
  }

  showTeam = () => {
    this.setState({lastCity: '', city: this.state.lastCity, showPlayer: false, player: {}});
  }

  render() {
    return (
      <ApolloProvider client={this.props.client}>
        <div>
          {this.state.city && <Players city={this.state.city} changePlayer={this.changePlayer} goBack={this.goHome} />}
          {this.state.showPlayer && <EditPlayer player={this.state.player} reset={this.resetPlayerView} goBack={this.showTeam} />}
          {!this.state.city && !this.state.showPlayer && <Teams handleClick={this.changeCity} />}
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
